import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Passing Data',
    home: TodoScreen(
      todos: List.generate(20, (i) => Todo('Todo $i', 'a description of what needs to be done for todo $i')),
    ),
  ));
}

class Todo {
  final String title;
  final String decription;

  const Todo(this.title, this.decription);
}

class TodoScreen extends StatelessWidget {
  const TodoScreen({Key? key, required this.todos}) : super(key: key);
  final List<Todo> todos;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Todos'),
      ),
      body: ListView.builder(
        itemCount: todos.length,
        itemBuilder: (context, index){
          return ListTile(
            title: Text(todos[index].title),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(
                builder: (context) => DetailScreen(todo: todos[index]),
                // settings: RouteSettings(
                //   arguments: todos[index],
                // )
                ),
              );
            },
          );
        },
      ),
    );
  }
}

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key, required this.todo}) : super(key: key);
  final Todo todo;
  @override
  Widget build(BuildContext context) {
    // final todo = ModalRoute.of(context)!.settings.arguments as Todo;
    return Scaffold(
      appBar: AppBar(
        title: Text(todo.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text(todo.decription),
      ),
    );
  }
}